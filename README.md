# Cookiecutter Extension

## Summary

> Create cookiecutter template under the current working directory of the Jupyter browser.

![Example](preview.gif)

> Credit: This example is based on the official jupyterlab services example [here](https://github.com/jupyterlab/jupyterlab/blob/869ed91e60f08e7e1c6787da27ec87255ced00bf/packages/services/examples/browser/src/contents.ts).

## Prerequisites

* JupyterLab >= 2.0.0
* nodejs >= 10.0.0

## Quick Start

### Installation

```bash
cd cookiecutter-extension
jlpm && jlpm run build
jupyter labextension install .
``` 

### Provided Docker Environment

* Base image: `jupyter/base-notebook:lab-2.1.5`

To build and run the container, simply:

```bash
docker-compose up --build --remove-orphans --no-deps
```

### Cookiecutter Template Structure

This repository is based on the [Cookiecutter Data Science template](https://drivendata.github.io/cookiecutter-data-science/#directory-structure). Dedicated folders for logs, unit testing and sql scripts has been added along with conda environment definition file. Requirements, setup and init files will all be created as empty files.

```
├── README.md          <- The top-level README for 
│                         developers using this project.
│
├── data
│   ├── external       <- Third party sources.
│   │   └── .gitkeep
│   ├── interim        <- In-progress intermediate data.
│   │   └── .gitkeep
│   ├── processed      <- The final data sets for modelling.
│   │   └── .gitkeep
│   └── raw            <- The original, immutable data.
│       └── .gitkeep
│
├── docs               <- A default Sphinx project; see 
│   │                     sphinx-doc.org for details
│   └── .gitkeep
│
├── environment.yml    <- Conda environment file
│
├── logs               <- Folder for storing logging outputs
│   └── .gitkeep
│
├── models             <- Trained and serialized models, model 
│   │                     predictions, or model summaries
│   └── .gitkeep
│
├── notebooks          <- Jupyter notebooks. Naming convention 
│   │                     is a number (for ordering), the
│   │                     creator's initials, and a short `-` 
│   │                     delimited description, e.g.
│   │                     `1.0-jqp-initial-data-exploration`.
│   └── .gitkeep
│
├── references         <- Data dictionaries, manuals, and all 
│   │                    other explanatory materials.
│   └── .gitkeep
│
├── reports            <- HTML, PDF, and LaTeX.
│   └── figures        <- Generated figures.
│       └── .gitkeep
│
├── requirements.txt   <- The requirements file for reproducing 
│                         the analysis environment, e.g.
│                         generated with `pip freeze > 
│                         requirements.txt` or `pipreqs src`
│
├── setup.py           <- Fill in the file to make this project 
│                         pip installable with `pip install -e`
│
├── sql                <- SQL scripts, stored procedures etc. This
|   |                     is also the folder where you define your
|   |                     dbt workflow.
│   └── .gitkeep
│
├── src                <- Source code for use in this project.
│   └── __init__.py    <- Makes src a Python module
│
├── tests              <- Scripts for unit testing
│   └── .gitkeep
│
└── .gitignore
```
