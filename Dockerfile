FROM jupyter/base-notebook:lab-2.1.5

USER root

RUN pip install --upgrade pip

COPY jupyter-cookiecutter-extension /jupyter-cookiecutter-extension
RUN cd /jupyter-cookiecutter-extension \
    && jlpm && jlpm run build \
    && jupyter labextension install . --no-build
RUN jupyter lab build --minimize=False

USER $NB_USER