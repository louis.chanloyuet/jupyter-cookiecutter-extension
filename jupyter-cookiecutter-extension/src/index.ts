import {
  JupyterFrontEnd,
  JupyterFrontEndPlugin
} from '@jupyterlab/application';

import { ICommandPalette } from '@jupyterlab/apputils';

import { IFileBrowserFactory } from '@jupyterlab/filebrowser';

import { ILauncher } from '@jupyterlab/launcher';

import { IMainMenu } from '@jupyterlab/mainmenu';

import { LabIcon } from '@jupyterlab/ui-components';

import { ContentsManager } from '@jupyterlab/services';

import cookieIconStr from '../style/cookie.svg';

// class CustomSaveModel implements Contents.IModel
// {
//   readonly type: Contents.ContentType = 'file'
//   readonly writable: boolean = true
//   readonly mimetype: string = 'application/json'
//   readonly format: Contents.FileFormat = 'text'

//   readonly name: string
//   readonly path: string
//   readonly created: string
//   readonly last_modified: string
//   readonly content: any

//   constructor(name: string, path: string, createDate: string, modDate: string, content: any)
//   {
//     this.name = name
//     this.path = path
//     this.created = createDate
//     this.last_modified = modDate
//     this.content = content
//   }
// }

// const FACTORY = 'Editor';
const PALETTE_CATEGORY = 'Extension Examples';

namespace CommandIDs {
  export const createNew = 'jlab-extension:create-cookiecutter-template';
}

const extension: JupyterFrontEndPlugin<void> = {
  id: 'launcher',
  autoStart: true,
  requires: [IFileBrowserFactory],
  optional: [ILauncher, IMainMenu, ICommandPalette],
  activate: (
    app: JupyterFrontEnd,
    browserFactory: IFileBrowserFactory,
    launcher: ILauncher | null,
    menu: IMainMenu | null,
    palette: ICommandPalette | null
  ) => {
    const { commands } = app;
    const command = CommandIDs.createNew;
    const icon = new LabIcon({
      name: 'launcher:cookie-icon',
      svgstr: cookieIconStr
    });

    commands.addCommand(command, {
      label: args => (args['isPalette'] ? 'New Cookiecutter Template' : 'Cookiecutter Template'),
      caption: 'Create cookiecutter template',
      icon: args => (args['isPalette'] ? null : icon),
      execute: async args => {
        // Get the directory in which the template must be created;
        // otherwise take the current filebrowser directory
        var cwd = args['cwd'] || browserFactory.defaultBrowser.model.path;
        if (cwd !== '') {
          cwd = '/' + cwd
        };
        
        const contents = new ContentsManager();
        await contents.get(cwd);

        // Source Folder

        const src = await contents.newUntitled({ 
          path: cwd, type: 'directory'
        });
        await contents.rename(src.path, cwd + '/src');
        const src_init = await contents.newUntitled({ 
          path: cwd + '/src', type: 'file', ext: 'py'
        });
        await contents.rename(src_init.path, cwd + '/src/__init__.py');

        // Docs Folder
        
        const data = await contents.newUntitled({ 
          path: cwd, type: 'directory'
        });
        await contents.rename(data.path, cwd + '/data');
        const data_external = await contents.newUntitled({ 
          path: cwd + '/data', type: 'directory'
        });
        await contents.rename(data_external.path, cwd + '/data/external');
        const data_external_gitkeep = await contents.newUntitled({ 
          path: cwd + '/data/external', type: 'file', ext: 'py'
        });
        await contents.rename(data_external_gitkeep.path, cwd + '/data/external/.gitkeep');
        const data_interim = await contents.newUntitled({ 
          path: cwd + '/data', type: 'directory'
        });
        await contents.rename(data_interim.path, cwd + '/data/interim');
        const data_interim_gitkeep = await contents.newUntitled({ 
          path: cwd + '/data/interim', type: 'file', ext: 'py'
        });
        await contents.rename(data_interim_gitkeep.path, cwd + '/data/interim/.gitkeep');
        const data_processed = await contents.newUntitled({ 
          path: cwd + '/data', type: 'directory'
        });
        await contents.rename(data_processed.path, cwd + '/data/processed');
        const data_processed_gitkeep = await contents.newUntitled({ 
          path: cwd + '/data/processed', type: 'file', ext: 'py'
        });
        await contents.rename(data_processed_gitkeep.path, cwd + '/data/processed/.gitkeep');
        const data_raw = await contents.newUntitled({ 
          path: cwd + '/data', type: 'directory'
        });
        await contents.rename(data_raw.path, cwd + '/data/raw');
        const data_raw_gitkeep = await contents.newUntitled({ 
          path: cwd + '/data/raw', type: 'file', ext: 'py'
        });
        await contents.rename(data_raw_gitkeep.path, cwd + '/data/raw/.gitkeep');

        // Docs Folder
        
        const docs = await contents.newUntitled({ 
          path: cwd, type: 'directory'
        });
        await contents.rename(docs.path, cwd + '/docs');
        const docs_gitkeep = await contents.newUntitled({ 
          path: cwd + '/docs', type: 'file', ext: 'py'
        });
        await contents.rename(docs_gitkeep.path, cwd + '/docs/.gitkeep');

        // Logs Folder
        
        const logs = await contents.newUntitled({ 
          path: cwd, type: 'directory'
        });
        await contents.rename(logs.path, cwd + '/logs');
        const logs_gitkeep = await contents.newUntitled({ 
          path: cwd + '/logs', type: 'file', ext: 'py'
        });
        await contents.rename(logs_gitkeep.path, cwd + '/logs/.gitkeep');

        // Models Folder
        
        const models = await contents.newUntitled({ 
          path: cwd, type: 'directory'
        });
        await contents.rename(models.path, cwd + '/models');
        const models_gitkeep = await contents.newUntitled({ 
          path: cwd + '/models', type: 'file', ext: 'py'
        });
        await contents.rename(models_gitkeep.path, cwd + '/models/.gitkeep');

        // Notebooks Folder
        
        const notebooks = await contents.newUntitled({ 
          path: cwd, type: 'directory'
        });
        await contents.rename(notebooks.path, cwd + '/notebooks');
        const notebooks_gitkeep = await contents.newUntitled({ 
          path: cwd + '/notebooks', type: 'file', ext: 'py'
        });
        await contents.rename(notebooks_gitkeep.path, cwd + '/notebooks/.gitkeep');

        // References Folder
        
        const references = await contents.newUntitled({ 
          path: cwd, type: 'directory'
        });
        await contents.rename(references.path, cwd + '/references');
        const references_gitkeep = await contents.newUntitled({ 
          path: cwd + '/references', type: 'file', ext: 'py'
        });
        await contents.rename(references_gitkeep.path, cwd + '/references/.gitkeep');

        // Reports Folder
        
        const reports = await contents.newUntitled({ 
          path: cwd, type: 'directory'
        });
        await contents.rename(reports.path, cwd + '/reports');
        const reports_gitkeep = await contents.newUntitled({ 
          path: cwd + '/reports', type: 'file', ext: 'py'
        });
        await contents.rename(reports_gitkeep.path, cwd + '/reports/.gitkeep');

        // SQL Folder
        
        const sql = await contents.newUntitled({ 
          path: cwd, type: 'directory'
        });
        await contents.rename(sql.path, cwd + '/sql');
        const sql_gitkeep = await contents.newUntitled({ 
          path: cwd + '/sql', type: 'file', ext: 'py'
        });
        await contents.rename(sql_gitkeep.path, cwd + '/sql/.gitkeep');

        // Tests Folder
        
        const tests = await contents.newUntitled({ 
          path: cwd, type: 'directory'
        });
        await contents.rename(tests.path, cwd + '/tests');
        const tests_gitkeep = await contents.newUntitled({ 
          path: cwd + '/tests', type: 'file', ext: 'py'
        });
        await contents.rename(tests_gitkeep.path, cwd + '/tests/.gitkeep');

        // Files

        const environment = await contents.newUntitled({
          path: cwd, type: 'file', ext: 'yml'
        });
        await contents.rename(environment.path, cwd + '/environment.yml');

        const readme = await contents.newUntitled({
          path: cwd, type: 'file', ext: 'md'
        });
        await contents.rename(readme.path, cwd + '/Readme.md');

        const requirements = await contents.newUntitled({
          path: cwd, type: 'file', ext: 'txt'
        });
        await contents.rename(requirements.path, cwd + '/requirements.txt');

        const setup = await contents.newUntitled({
          path: cwd, type: 'file', ext: 'py'
        });
        await contents.rename(setup.path, cwd + '/setup.py');

        const m = await contents.newUntitled({
          path: cwd, type: 'file', ext: 'py'
        });
        await contents.rename(m.path, cwd + '/.gitignore');
        // console.log(m); 
        await contents.save(cwd + '/.gitignore', {...m,  
          content: 
            // Byte-compiled / optimized / DLL files
            '# Byte-compiled / optimized / DLL files\n' + 
            '__pycache__/\n*.py[cod]\n*$py.class\n\n' + 

            // C extensions
            '# C extensions\n' + 
            '*.so\n\n' + 

            // Distribution / packaging
            '# Distribution / packaging\n' + 
            '.Python\nbuild/\ndevelop-eggs/\ndist/\ndownloads/\neggs/\n.eggs/\nlib/\n' + 
            'lib64/\nparts/\nsdist/\nvar/\nwheels/\nshare/python-wheels/\n*.egg-info/\n' + 
            '.installed.cfg\n*.egg\nMANIFEST\n\n' + 

            // PyInstaller
            '# PyInstaller\n' + 
            '*.manifest\n*.spec\n\n' + 

            // Installer logs
            '# Installer logs\n' + 
            'pip-log.txt\npip-delete-this-directory.txt\n\n' + 

            // Unit test / coverage reports
            '# Unit test / coverage reports\n' + 
            'htmlcov/\n.tox/\n.nox/\n.coverage\n.coverage.*\n.cache\nnosetests.xml\n' + 
            'coverage.xml\n*.cover\n*.py,cover\n.hypothesis/\n.pytest_cache/\ncover/\n\n' + 

            // Translations
            '# \n' + 
            '*.mo\n*.pot\n\n' + 

            // Django stuff
            '# \n' + 
            '*.log\nlocal_settings.py\ndb.sqlite3\ndb.sqlite3-journal\n\n' + 

            // Flask stuff
            '# \n' + 
            'instance/\n.webassets-cache\n\n' + 

            // Scrapy stuff
            '# \n' + 
            '.scrapy\n\n' + 

            // Sphinx documentation
            '# \n' + 
            'docs/_build/\n\n' + 

            // PyBuilder
            '# \n' + 
            '.pybuilder/\ntarget/\n\n' + 

            // Jupyter Notebook
            '# \n' + 
            '.ipynb_checkpoints\n*/.ipynb_checkpoints/*\n\n' + 

            // IPython
            '# \n' + 
            'profile_default/\nipython_config.py\n\n' + 

            // PEP 582
            '# \n' + 
            '__pypackages__/\n\n' + 

            // Celery stuff
            '# \n' + 
            'celerybeat-schedule\ncelerybeat.pid\n\n' + 

            // SageMath parsed files
            '# \n' + 
            '*.sage.py\n\n' + 

            // Environments
            '# \n' + 
            '.env\n.venv\nenv/\nvenv/\nENV/\nenv.bak/\nvenv.bak/\n\n' + 

            // Spyder project settings
            '# \n' + 
            '.spyderproject\n.spyproject\n\n' + 

            // Rope project settings
            '# \n' + 
            '.ropeproject\n\n' + 

            // mkdocs documentation
            '# \n' + 
            '/site\n\n' + 

            // mypy
            '# \n' + 
            '.mypy_cache/\n.dmypy.json\ndmypy.json\n\n' + 

            // Pyre type check
            '# \n' + 
            '.pyre/\n\n' + 

            // pytype static type analyzer
            '# \n' + 
            '.pytype/\n\n' + 

            // Cython debug symbols
            '# \n' + 
            'cython_debug/\n',
          type: 'file',
          format: 'text'});
        return;
      }
    });

    // Add the command to the launcher
    if (launcher) {
      launcher.add({
        command,
        category: 'Custom Extensions',
        rank: 1
      });
    }

    // Add the command to the palette
    if (palette) {
      palette.addItem({
        command,
        args: { isPalette: true },
        category: PALETTE_CATEGORY
      });
    }

    // Add the command to the menu
    if (menu) {
      menu.fileMenu.newMenu.addGroup([{ command }], 30);
    }
  }
};

export default extension;
